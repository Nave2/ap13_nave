#pragma once
#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include "Helper.h"
#include <thread>
#include <fstream>
#include <functional>

#include <mutex>

using namespace std;

mutex fileMtx;

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	cout << "Starting..." << endl;
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (_WINSOCK2API_::bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	cout << "binded" << endl;

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	cout << "listening..." << endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accepting client..." << std::endl;

		thread msgs(&Server::handleMsgs, this);
		msgs.detach();

		accept();
	}
}

string Server::getUsersList() const
{
	string usersList;
	for (string userInList : this->_users)
		usersList += userInList + "&";
	usersList = usersList.substr(0, usersList.length() - 1);
	return usersList;
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	//std::cout << "Client accepted. Server and client can speak" << std::endl;
	string username;
	try
	{
		int msgCode = Helper::getMessageTypeCode(client_socket);
		if (msgCode == MT_CLIENT_LOG_IN) //just a doublecheck
		{
			int nameSize = Helper::getIntPartFromSocket(client_socket, 2);
			username = Helper::getStringPartFromSocket(client_socket, nameSize);
			this->_users.insert(username);
			cout << "ADDED new client, " << username << ", to clients list" << endl;

			Helper::send_update_message_to_client(client_socket, "", "", this->getUsersList());
		}
		else throw exception("something smells fishy");
	}
	catch (exception & e)
	{
		// client disconnected
		this->_users.erase(username);
		cout << username << " has just disconnected." << endl;
		return;
	}

	thread clientThread(&Server::clientHandler, this, client_socket, username);
	clientThread.detach();
}

string getFileName(string from, string to)
{
	return (from < to) ? from + "&" + to : to + "&" + from;
}

string getFileContent(string path)
{
	string line, content;
	ifstream file(path + ".txt", ios::out | std::ios::app);
	while (getline(file, line))
		content += line;
	file.close();
	return content;
}
void writeMsgToFile(Message msg)
{
	string filename = getFileName(msg.from, msg.to);
	ofstream chatFOut(filename + ".txt", ios::out | std::ios::app);
	chatFOut << "&MAGSH_MESSAGE&&Author&" << msg.from << "&DATA&" << msg.message;
	chatFOut.close();
}

void Server::handleMsgs()
{
	while (true)
	{
		unique_lock<mutex> lock(this->_msgsMtx);
		this->_msgsQueueCV.wait(lock, std::bind(&Server::_msgsPushed, this));
		Message msg = this->_msgs.front();
		this->_msgs.pop();
		writeMsgToFile(msg);
		this->_msgsPushed = !this->_msgs.empty();
	}
}

void Server::clientHandler(SOCKET clientSocket, string username)
{
	try
	{
		int msgCode = 0;
		while (msgCode != MT_CLIENT_EXIT)
		{
			msgCode = Helper::getMessageTypeCode(clientSocket);

			if (msgCode != MT_CLIENT_EXIT)
			{
				//code = MT_CLIENT_UPDATE
				string secondUser = getNextData(clientSocket, 2);
				string newMsg = getNextData(clientSocket, 5);

				string chatHistory = "";
				if (secondUser.length() != 0) // request about a chat with specific user
				{
					if (newMsg.length() != 0) {
						lock_guard<mutex> lock(this->_msgsMtx);
						Message msg = { username, secondUser, newMsg };
						this->_msgs.push(msg);
						this->_msgsPushed = true;
						this->_msgsQueueCV.notify_one();
					}
					chatHistory = getFileContent(getFileName(username, secondUser));
				}
				Helper::send_update_message_to_client(clientSocket, chatHistory, secondUser, this->getUsersList());
			}
		}
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception & e)
	{
		// client disconnected
		this->_users.erase(username);
		cout << username << " has just disconnected." << endl;
		closesocket(clientSocket);
	}


}

string Server::getNextData(SOCKET clientSocket, int dataSizeLen)
{
	int len = Helper::getIntPartFromSocket(clientSocket, dataSizeLen);
	string content = Helper::getStringPartFromSocket(clientSocket, len);
	return content;
}