#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <fstream>
#include <string>
#include <regex>
#include <map>

using namespace std;

int main()
{

	// get config
	map<string, string> config;
	ifstream configF;
	configF.open("config.txt");

	string line;
	while (getline(configF, line))
	{
		string name = line.substr(0, line.find("=", 0));
		string value = line.substr(line.find("=", 0) + 1);
		config[name] = value;
	}
	configF.close();
	
	// run server
	try {
		// set WSAInitializer
		WSAInitializer wsaInit;
		// set up the socket
		Server server;
		server.serve(stoi(config["port"]));
	}
	catch (exception & e) {
		cout << "server stopped working." << endl;
		system("PAUSE");
		return 1;
	}

	system("PAUSE");
	return 0;
}