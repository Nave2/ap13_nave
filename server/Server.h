#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <set>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

// message to push to queue
typedef struct Message {
	string from;
	string to;
	string message;
} Message;


class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void accept();
	string getNextData(SOCKET clientSocket, int dataSizeLen);
	void clientHandler(SOCKET clientSocket, string username);
	void handleMsgs();
	string getUsersList() const;

	SOCKET _serverSocket;
	set<string> _users;
	queue<Message> _msgs;
	mutex _msgsMtx;
	condition_variable _msgsQueueCV;
	bool _msgsPushed;
};